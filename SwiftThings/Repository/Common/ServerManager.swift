//
//  ServerManager.swift
//  ReactiveMVVM
//
//  Created by Manuel Espeso on 22/2/22.
//

import Foundation
import Combine

class ServerManager {
    
    func request(_ endpoint: String,
                 method: RequestHTTPMethod = .get,
                 parameters: [String: Any]? = nil) -> AnyPublisher<Data, Error> {
        do {
            let url = try endpoint.asURL()
            
            var request = URLRequest(url: url)
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            //request.addValue(token, forHTTPHeaderField: "Authorization")
            
            request.httpMethod = method.rawValue
            
            print("------------------------------------------\n🌍 [REQUEST][URL]\n \(endpoint):")
            if let params = parameters,
               let data = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted),
               let text = String(data: data, encoding: .utf8) {
                print("[REQUEST] PARAMS ✅ \n\(text)")
                request.httpBody = data //parameters.percentEncoded()
            }
            print("------------------------------------------")
            
            return URLSession
                .shared
                .dataTaskPublisher(for: request)
                .mapError({ RequestMapper.shared.handleRequestError($0) })
                .map(\.data)
                .prettyRequestJSONFormat()
                .receive(on: DispatchQueue.main)
                .eraseToAnyPublisher()
        } catch let error {
            return Fail(error: error).eraseToAnyPublisher()
        }
    }
}
