//
//  CombineRepository.swift
//  ReactiveMVVM
//
//  Created by Manuel Espeso on 21/2/22.
//

import Combine

protocol CombineRepository {
    func fetchUsers() -> AnyPublisher<[User], Error>
}

class CombineRepositoryDefault: ServerManager, CombineRepository {
    
    func fetchUsers() -> AnyPublisher<[User], Error> {
        request("https://jsonplaceholder.typicode.com/users")
            .decode(as: [User].self)
            .validate(using: { users in
                guard users.count > 0 else {
                    throw RequestError.notData
                }
            })
            .eraseToAnyPublisher()
    }
}
