//
//  ViewController+Swizzle.swift
//  SwiftThings
//
//  Created by Manuel Espeso on 20/4/22.
//

import Foundation

extension UIViewController {
    
    static func swizzleViewWillAppear() {
        let selector1 = #selector(UIViewController.viewWillAppear(_:))
        let selector2 = #selector(UIViewController._swizzled_viewWillAppear(_:))
        let originalMethod = class_getInstanceMethod(UIViewController.self, selector1)!
        let swizzleMethod = class_getInstanceMethod(UIViewController.self, selector2)!
        
        method_exchangeImplementations(originalMethod, swizzleMethod)
    }
    
    @objc dynamic func _swizzled_viewWillAppear(_ animated: Bool) {
        _swizzled_viewWillAppear(animated)
        //do your stuffs
        switch self {
            case is ViewController: print("main")
            case is ShapesViewController: print("shapes")
            case is CustomActionSheetDemoViewController: print("sheet's")
            case is CustomSheetDetentViewController: print("sheet dentent")
            case is BottomSheetViewController: print("bottom sheet custom")
            case is CombineViewController: print("combine")
            default: break
        }
    }
}
