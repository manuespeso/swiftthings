//
//  Publiser+Decode.swift
//  ReactiveMVVM
//
//  Created by Manuel Espeso on 21/2/22.
//

import Foundation
import Combine
// Convierte las propiedades del JSON escritas en snake case(user_info) en camel case(userInfo)
extension JSONDecoder {
    static let snakeCaseConverting: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return decoder
    }()
}
// extension que permite simplificar el decode dandole uno por defecto y añadiendole el tipo de dato a decodear
extension Publisher where Output == Data {
    func decode<T: Decodable>(
        as type: T.Type = T.self,
        using decoder: JSONDecoder = .snakeCaseConverting
    ) -> Publishers.Decode<Self, T, JSONDecoder> {
        decode(type: type, decoder: decoder)
    }
}
