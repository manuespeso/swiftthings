//
//  Publisher+JSONFormat.swift
//  ReactiveMVVM
//
//  Created by Manuel Espeso on 23/2/22.
//

import Foundation
import Combine

extension Publisher where Output == Data {
    func prettyRequestJSONFormat() -> Publishers.Breakpoint<Self> {
        self.breakpoint(receiveOutput: { data in
            let jsonSerialized = try? JSONSerialization.jsonObject(with: data, options: [])
            /*
                {
                    "someKey": 42.0,
                    "anotherKey": {
                        "someNestedKey": true
                    }
                }
            */
            if let json = jsonSerialized as? [Any],
               let data = try? JSONSerialization.data(withJSONObject: json, options: .prettyPrinted),
               let text = String(data: data, encoding: .utf8) {
                Swift.print(" ✅ \(text)")
            }
            /*
                [
                    "hello", 3, true
                ]
            */
            if let json = jsonSerialized as? [String: Any],
               let data = try? JSONSerialization.data(withJSONObject: json, options: .prettyPrinted),
               let text = String(data: data, encoding: .utf8) {
                Swift.print(" ✅ \(text)")
            }
            
            return false
        })
    }
}
