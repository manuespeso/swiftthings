//
//  Publisher+Convert.swift
//  ReactiveMVVM
//
//  Created by Manuel Espeso on 21/2/22.
//

import Foundation
import Combine

/*
 - Mapea el result/output del publisher
 - Si es posible devuelve un Result.success o LoadableState.loaded con el output
 - Si no es posible hace un catch del error para obtenerlo y devolverlo
*/

extension Publisher {
    func convertToResult() -> AnyPublisher<Result<Output, Failure>, Never> {
        self.map(Result.success)
            .catch { Just(.failure($0)) }
            .eraseToAnyPublisher()
    }
}

extension Publisher {
    func convertToLoadedState() -> AnyPublisher<LoadableState<Output>, Never> {
        self.map(LoadableState.loaded)
            .catch { Just(.failed($0)) }
            .eraseToAnyPublisher()
    }
}
