//
//  Publisher+Validate.swift
//  ReactiveMVVM
//
//  Created by Manuel Espeso on 21/2/22.
//

import Combine
// Se usa un validator, que no es mas que una clousure al que se le pasa como argumento el output
// con ese output de la clousure se realiza la validacion que se le quiera dar
// si en la validacion no se ha lanzado un throw, el try no termina la ejecucion y se devuelve el output con normalidad
extension Publisher {
    func validate(using validator: @escaping (Output) throws -> Void) -> Publishers.TryMap<Self, Output> {
        tryMap { output in
            try validator(output)
            return output
        }
    }
}
