//
//  Control+Publisher.swift
//  ReactiveMVVM
//
//  Created by Manuel Espeso on 2/3/22.
//

import UIKit
import Combine

extension UIControl {
    
    class InteractionSubscription<S: Subscriber>: Subscription where S.Input == UIControl {
        
        private var currentDemand: Subscribers.Demand = .none
        private var subscriber: S?
        private let control: UIControl
        private let event: UIControl.Event
        
        init(subscriber: S, control: UIControl, event: UIControl.Event) {
            self.subscriber = subscriber
            self.control = control
            self.event = event
            
            self.control.addTarget(self, action: #selector(handleEvent), for: event)
        }
        
        @objc private func handleEvent(_ sender: UIControl) {
            if currentDemand > 0 {
                currentDemand += subscriber?.receive(control) ?? .none
                currentDemand -= 1
            }
        }
        
        func request(_ demand: Subscribers.Demand) {
            currentDemand += demand
        }
        
        func cancel() {
            // free-up any allocated ressources
            subscriber = nil
            control.removeTarget(self, action: #selector(handleEvent), for: event) // reuse the event
        }
    }
    
    struct InteractionPublisher: Publisher {
        
        typealias Output = UIControl
        typealias Failure = Never
        
        private let control: UIControl
        private let event: UIControl.Event
        
        init(control: UIControl, event: UIControl.Event) {
            self.control = control
            self.event = event
        }
        
        func receive<S>(subscriber: S) where S : Subscriber, Output == S.Input, Never == S.Failure {
            let subscription = InteractionSubscription(subscriber: subscriber, control: control, event: event)
            subscriber.receive(subscription: subscription)
        }
    }
    
    func publisher(for event: UIControl.Event) -> UIControl.InteractionPublisher {
        InteractionPublisher(control: self, event: event)
    }
}
