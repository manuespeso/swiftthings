//
//  UIColor+AppColors.swift
//  SwiftThings
//
//  Created by Manuel Espeso on 21/4/22.
//

import Foundation

extension UIColor {
    
    static var cornflowerBlue: UIColor {
        UIColor(displayP3Red: 100.0 / 255.0, green: 149.0 / 255.0, blue: 237.0 / 255.0, alpha: 1.0)
    }
}
