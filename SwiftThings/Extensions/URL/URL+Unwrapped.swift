//
//  URL+Unwrapped.swift
//  ReactiveMVVM
//
//  Created by Manuel Espeso on 21/2/22.
//

import Foundation

extension URL {
    init(_ string: StaticString) {
        self.init(string: "\(string)")!
    }
}
