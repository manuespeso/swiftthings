//
//  String+ToURL.swift
//  ReactiveMVVM
//
//  Created by Manuel Espeso on 23/2/22.
//

import Foundation

extension String {
    /// Returns a `URL` if `self` can be used to initialize a `URL` instance, otherwise throws.
    ///
    /// - Returns: The `URL` initialized with `self`.
    /// - Throws:  An `RequestError.incorrectURL` instance.
    func asURL() throws -> URL {
        guard let url = URL(string: self) else { throw RequestError.incorrectURL }
        return url
    }
}
