//
//  LabelCell.swift
//  SwiftThings
//
//  Created by Manuel Espeso on 22/4/22.
//

import UIKit

class LabelCell: UICollectionViewCell {
    
    static let reuseIdentifier = "label-cell-reuse-identifier"
    
    let label = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("not implemented")
    }
    
    private func setupUI() {
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.preferredFont(forTextStyle: .body)
        label.adjustsFontForContentSizeCategory = true
        
        contentView.addSubview(label)
        layer.borderWidth = 1
        layer.borderColor = UIColor.systemGray2.cgColor
        
        let inset = CGFloat(10)
        NSLayoutConstraint.activate([
            label.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: inset),
            label.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -inset),
            label.centerYAnchor.constraint(equalTo: contentView.centerYAnchor)
        ])
    }
}
