//
//  ConferenceVideoSessionsViewController.swift
//  SwiftThings
//
//  Created by Manuel Espeso on 21/4/22.
//

import UIKit

class ConferenceVideoSessionsViewController: UIViewController {
    
    private var currentSnapshot = NSDiffableDataSourceSnapshot<VideoCollection, Video>()
    private let videosController = ConferenceVideoController()
    private let titleElementKind = "title-element-kind"
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: createLayout())
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collectionView.backgroundColor = .systemBackground
        
        view.addSubview(collectionView)
        
        return collectionView
    }()
    
    private lazy var dataSource: UICollectionViewDiffableDataSource<VideoCollection, Video> = {
        let cellRegistration = UICollectionView.CellRegistration<ConferenceVideoCell, Video> { (cell, _, video) in
            cell.titleLabel.text = video.title
            cell.categoryLabel.text = video.category
        }
        
        let dataSource = UICollectionViewDiffableDataSource<VideoCollection, Video>(collectionView: collectionView) {
            (collectionView, indexPath, video) -> UICollectionViewCell? in
            collectionView.dequeueConfiguredReusableCell(using: cellRegistration, for: indexPath, item: video)
        }
        
        let supplementaryRegistration = UICollectionView.SupplementaryRegistration<TitleSupplementaryView>(elementKind: titleElementKind) {
            (supplementaryView, _, indexPath) in
            let videoCategory = self.currentSnapshot.sectionIdentifiers[indexPath.section]
            supplementaryView.label.text = videoCategory.title
        }
        
        dataSource.supplementaryViewProvider = { (_, _, index) in
            self.collectionView.dequeueConfiguredReusableSupplementary(using: supplementaryRegistration, for: index)
        }
        
        return dataSource
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Conference Videos"
        
        initialSnapshot()
    }
    
    private func initialSnapshot() {
        videosController.collections.forEach {
            currentSnapshot.appendSections([$0])
            currentSnapshot.appendItems($0.videos)
        }
        
        dataSource.apply(currentSnapshot, animatingDifferences: false)
    }
    
    private func createLayout() -> UICollectionViewLayout {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupFractionalWidth = CGFloat(0.85)
        let groupSize = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(groupFractionalWidth),
            heightDimension: .absolute(250)
        )
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        
        let section = NSCollectionLayoutSection(group: group)
        section.orthogonalScrollingBehavior = .continuous
        section.interGroupSpacing = 20
        section.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20)
        
        let titleSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .estimated(44))
        let titleSupplementary = NSCollectionLayoutBoundarySupplementaryItem(
            layoutSize: titleSize,
            elementKind: titleElementKind,
            alignment: .top)
        section.boundarySupplementaryItems = [titleSupplementary]
        
        let config = UICollectionViewCompositionalLayoutConfiguration()
        config.interSectionSpacing = 20
        
        let layout = UICollectionViewCompositionalLayout(section: section, configuration: config)
        
        return layout
    }
}
