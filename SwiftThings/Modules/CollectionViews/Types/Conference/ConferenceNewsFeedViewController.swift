//
//  ConferenceNewsFeedViewController.swift
//  SwiftThings
//
//  Created by Manuel Espeso on 21/4/22.
//

import UIKit

class ConferenceNewsFeedViewController: UIViewController {
    
    private let newsController = ConferenceNewsController()
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: createLayout())
        collectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        collectionView.backgroundColor = .systemBackground
        
        view.addSubview(collectionView)
        
        return collectionView
    }()
    
    private lazy var dataSource: UICollectionViewDiffableDataSource<Int, NewsFeedItem> = {
        let cellRegistration = UICollectionView.CellRegistration<ConferenceNewsFeedCell, NewsFeedItem> { (cell, indexPath, newsItem) in
            cell.titleLabel.text = newsItem.title
            cell.bodyLabel.text = newsItem.body
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .short
            cell.dateLabel.text = dateFormatter.string(from: newsItem.date)
            cell.showsSeparator = indexPath.item != self.newsController.items.count - 1
        }
        
        let dataSource = UICollectionViewDiffableDataSource<Int, NewsFeedItem>(collectionView: collectionView) {
            (collectionView, indexPath, item) -> UICollectionViewCell? in
            collectionView.dequeueConfiguredReusableCell(using: cellRegistration, for: indexPath, item: item)
        }
        
        return dataSource
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Conference News Feed"
        
        initialSnapshot()
    }
    
    func initialSnapshot() {
        var snapshot = NSDiffableDataSourceSnapshot<Int, NewsFeedItem>()
        snapshot.appendSections([0])
        snapshot.appendItems(newsController.items)
        
        dataSource.apply(snapshot, animatingDifferences: false)
    }
    
    func createLayout() -> UICollectionViewLayout {
        let layoutSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .estimated(100))
        let item = NSCollectionLayoutItem(layoutSize: layoutSize)
        
        let group = NSCollectionLayoutGroup.horizontal(
            layoutSize: layoutSize,
            subitem: item,
            count: 1
        )
        
        let section = NSCollectionLayoutSection(group: group)
        section.contentInsets = NSDirectionalEdgeInsets(top: 10, leading: 10, bottom: 10, trailing: 10)
        section.interGroupSpacing = 10
        
        let layout = UICollectionViewCompositionalLayout(section: section)
        
        return layout
    }
}
