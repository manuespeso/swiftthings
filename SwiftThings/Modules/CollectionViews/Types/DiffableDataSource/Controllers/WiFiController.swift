//
//  WiFiController.swift
//  SwiftThings
//
//  Created by Manuel Espeso on 22/4/22.
//

import Foundation

struct Network: Hashable {
    let name: String
    let identifier = UUID()
    
    static func == (lhs: Network, rhs: Network) -> Bool {
        lhs.identifier == rhs.identifier
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }
}

class WiFiController {
    
    typealias UpdateHandler = () -> Void
    
    var wifiEnabled = true
    var availableNetworks = Set<Network>()
    
    private var _availableNetworksDict = [UUID: Network]()
    private let updateInterval = 2000
    private let updateHandler: UpdateHandler
    private let allNetworks = [
        Network(name: "AirSpace1"),
        Network(name: "Living Room"),
        Network(name: "Courage"),
        Network(name: "Nacho WiFi"),
        Network(name: "FBI Surveillance Van"),
        Network(name: "Peacock-Swagger"),
        Network(name: "GingerGymnist"),
        Network(name: "Second Floor"),
        Network(name: "Evergreen"),
        Network(name: "__hidden_in_plain__sight__"),
        Network(name: "MarketingDropBox"),
        Network(name: "HamiltonVille"),
        Network(name: "404NotFound"),
        Network(name: "SNAGVille"),
        Network(name: "Overland101"),
        Network(name: "TheRoomWiFi"),
        Network(name: "PrivateSpace")
    ]
    
    init(updateHandler: @escaping UpdateHandler) {
        self.updateHandler = updateHandler
        
        availableNetworks = Set<Network>(allNetworks)
        performRandomUpdate()
    }
    
    private func performRandomUpdate() {
        if wifiEnabled {
            var updatedNetworks = Array(availableNetworks)
            
            if updatedNetworks.isEmpty {
                availableNetworks = Set<Network>(allNetworks)
            } else {
                let shouldRemove = Int.random(in: 0..<3) == 0
                if shouldRemove {
                    let removeCount = Int.random(in: 0..<updatedNetworks.count)
                    for _ in 0..<removeCount {
                        let removeIndex = Int.random(in: 0..<updatedNetworks.count)
                        updatedNetworks.remove(at: removeIndex)
                    }
                }
                
                let shouldAdd = Int.random(in: 0..<3) == 0
                if shouldAdd {
                    let allNetworksSet = Set<Network>(allNetworks)
                    var updatedNetworksSet = Set<Network>(updatedNetworks)
                    let notPresentNetworksSet = allNetworksSet.subtracting(updatedNetworksSet)
                    
                    if !notPresentNetworksSet.isEmpty {
                        let addCount = Int.random(in: 0..<notPresentNetworksSet.count)
                        var notPresentNetworks = [Network](notPresentNetworksSet)
                        
                        for _ in 0..<addCount {
                            let removeIndex = Int.random(in: 0..<notPresentNetworks.count)
                            let networkToAdd = notPresentNetworks[removeIndex]
                            notPresentNetworks.remove(at: removeIndex)
                            updatedNetworksSet.insert(networkToAdd)
                        }
                    }
                    updatedNetworks = [Network](updatedNetworksSet)
                }
                availableNetworks = Set<Network>(updatedNetworks)
            }
            // notify
            updateHandler()
        }
        
        let deadline = DispatchTime.now() + DispatchTimeInterval.milliseconds(updateInterval)
        DispatchQueue.main.asyncAfter(deadline: deadline) { self.performRandomUpdate() }
    }
}
