//
//  MountainsViewController.swift
//  SwiftThings
//
//  Created by Manuel Espeso on 22/4/22.
//

import UIKit

class MountainsViewController: UIViewController {
    
    private lazy var dataSource: UICollectionViewDiffableDataSource<Int, Mountain> = {
        let cellRegistration = UICollectionView.CellRegistration<LabelCell, Mountain> { (cell, _, mountain) in
            cell.label.text = mountain.name
        }
        
        let dataSource = UICollectionViewDiffableDataSource<Int, Mountain>(collectionView: mountainsCollectionView) {
            (collectionView, indexPath, identifier) -> UICollectionViewCell? in
            collectionView.dequeueConfiguredReusableCell(using: cellRegistration, for: indexPath, item: identifier)
        }
        
        return dataSource
    }()
    
    private var mountainsCollectionView: UICollectionView!
    private let mountainsController = MountainsController()
    private let searchBar = UISearchBar(frame: .zero)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Mountains Search"
        
        setupUI()
        performQuery(with: nil)
    }
    
    private func setupUI() {
        view.backgroundColor = .systemBackground
        
        mountainsCollectionView = UICollectionView(frame: view.bounds, collectionViewLayout: createLayout())
        mountainsCollectionView.backgroundColor = .systemBackground
        mountainsCollectionView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        mountainsCollectionView.translatesAutoresizingMaskIntoConstraints = false
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(mountainsCollectionView)
        view.addSubview(searchBar)
        
        let views = ["cv": mountainsCollectionView!, "searchBar": searchBar]
        
        var constraints = [NSLayoutConstraint]()
        constraints.append(contentsOf: NSLayoutConstraint.constraints(
            withVisualFormat: "H:|[cv]|", options: [], metrics: nil, views: views)
        )
        constraints.append(contentsOf: NSLayoutConstraint.constraints(
            withVisualFormat: "H:|[searchBar]|", options: [], metrics: nil, views: views)
        )
        constraints.append(contentsOf: NSLayoutConstraint.constraints(
            withVisualFormat: "V:[searchBar]-20-[cv]|", options: [], metrics: nil, views: views)
        )
        constraints.append(searchBar.topAnchor.constraint(
            equalToSystemSpacingBelow: view.safeAreaLayoutGuide.topAnchor, multiplier: 1.0)
        )
        NSLayoutConstraint.activate(constraints)
        
        searchBar.delegate = self
    }
    
    private func performQuery(with filter: String?) {
        let mountains = mountainsController.filteredMountains(with: filter).sorted { $0.name < $1.name }
        
        var snapshot = NSDiffableDataSourceSnapshot<Int, Mountain>()
        snapshot.appendSections([0])
        snapshot.appendItems(mountains)
        dataSource.apply(snapshot, animatingDifferences: true)
    }
    
    private func createLayout() -> UICollectionViewLayout {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(32))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 2)
        group.interItemSpacing = .fixed(10)
        
        let section = NSCollectionLayoutSection(group: group)
        section.interGroupSpacing = 10
        section.contentInsets = NSDirectionalEdgeInsets(top: 10, leading: 10, bottom: 10, trailing: 10)
        
        let layout = UICollectionViewCompositionalLayout(section: section)
        
        return layout
    }
}

extension MountainsViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        performQuery(with: searchText)
    }
}

