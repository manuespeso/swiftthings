//
//  WiFiSettingsViewController.swift
//  SwiftThings
//
//  Created by Manuel Espeso on 22/4/22.
//

import UIKit

enum WifiSection: CaseIterable {
    case config, networks
}

enum WifiItemType {
    case wifiEnabled, currentNetwork, availableNetwork
}

struct WifiItem: Hashable {
    let title: String
    let type: WifiItemType
    let network: Network?
    private let identifier: UUID
    
    var isConfig: Bool {
        let configItems: [WifiItemType] = [.currentNetwork, .wifiEnabled]
        return configItems.contains(type)
    }
    
    var isNetwork: Bool {
        type == .availableNetwork
    }
    
    init(title: String, type: WifiItemType) {
        self.title = title
        self.type = type
        self.network = nil
        self.identifier = UUID()
    }
    
    init(network: Network) {
        self.title = network.name
        self.type = .availableNetwork
        self.network = network
        self.identifier = network.identifier
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.identifier)
    }
}

class WiFiSettingsViewController: UIViewController {
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: view.bounds, style: .insetGrouped)
        view.addSubview(tableView)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
        
        return tableView
    }()
    
    private lazy var configurationItems: [WifiItem] = {
        [WifiItem(title: "Wi-Fi", type: .wifiEnabled), WifiItem(title: "breeno-net", type: .currentNetwork)]
    }()
    
    private lazy var dataSource: UITableViewDiffableDataSource<WifiSection, WifiItem> = {
        let dataSource = UITableViewDiffableDataSource<WifiSection, WifiItem>(tableView: tableView) { [weak self]
            (tableView, indexPath, item) -> UITableViewCell? in
            guard let self = self else { return nil }
            
            let cell = tableView.dequeueReusableCell(withIdentifier: self.reuseIdentifier, for: indexPath)
            var content = cell.defaultContentConfiguration()
            // network cell
            if item.isNetwork {
                content.text = item.title
                cell.accessoryType = .detailDisclosureButton
                cell.accessoryView = nil
                // configuration cells
            } else if item.isConfig {
                content.text = item.title
                
                if item.type == .wifiEnabled {
                    let enableWifiSwitch = UISwitch()
                    enableWifiSwitch.isOn = self.wifiController.wifiEnabled
                    enableWifiSwitch.addTarget(self, action: #selector(self.toggleWifi(_:)), for: .touchUpInside)
                    cell.accessoryView = enableWifiSwitch
                } else {
                    cell.accessoryView = nil
                    cell.accessoryType = .detailDisclosureButton
                }
            }
            cell.contentConfiguration = content
            
            return cell
        }
        dataSource.defaultRowAnimation = .fade
        
        return dataSource
    }()
    
    private var wifiController: WiFiController!
    private let reuseIdentifier = "reuse-identifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Wi-Fi"
        
        setupUI()
        updateUI(animated: true)
    }
    
    private func setupUI() {
        wifiController = WiFiController { [weak self] in self?.updateUI() }
    }
    
    private func updateUI(animated: Bool = true) {
        guard let wifiController = wifiController else { return }
        
        let configItems = configurationItems.filter { !($0.type == .currentNetwork && !wifiController.wifiEnabled) }
        var currentSnapshot = NSDiffableDataSourceSnapshot<WifiSection, WifiItem>()
        currentSnapshot.appendSections([.config])
        currentSnapshot.appendItems(configItems, toSection: .config)
        
        if wifiController.wifiEnabled {
            let sortedNetworks = wifiController.availableNetworks.sorted { $0.name < $1.name }
            let networkItems = sortedNetworks.map { WifiItem(network: $0) }
            currentSnapshot.appendSections([.networks])
            currentSnapshot.appendItems(networkItems, toSection: .networks)
        }
        
        dataSource.apply(currentSnapshot, animatingDifferences: animated)
    }
    
    @objc private func toggleWifi(_ wifiEnabledSwitch: UISwitch) {
        wifiController.wifiEnabled = wifiEnabledSwitch.isOn
        updateUI()
    }
}
