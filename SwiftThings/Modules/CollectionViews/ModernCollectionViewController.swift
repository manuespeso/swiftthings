//
//  ModernCollectionViewController.swift
//  SwiftThings
//
//  Created by Manuel Espeso on 21/4/22.
//

import UIKit

fileprivate struct OutlineItem: Hashable {
    
    let title: String
    let subitems: [OutlineItem]
    let outlineViewController: UIViewController.Type?
    private let identifier = UUID()
    
    init(title: String,
         subitems: [OutlineItem] = [],
         viewController: UIViewController.Type? = nil) {
        self.title = title
        self.subitems = subitems
        self.outlineViewController = viewController
    }
    
    static func == (lhs: OutlineItem, rhs: OutlineItem) -> Bool {
        lhs.identifier == rhs.identifier
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }
}

class ModernCollectionViewController: UIViewController {
    
    private typealias CollectionDataSource = UICollectionViewDiffableDataSource<Int /*Section*/, OutlineItem /*Content*/>
    
    private lazy var outlineCollectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: view.bounds, collectionViewLayout: generateLayout())
        view.addSubview(collectionView)
        collectionView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        collectionView.backgroundColor = .systemGroupedBackground
        collectionView.delegate = self
        
        return collectionView
    }()
    
    private lazy var dataSource: CollectionDataSource = {
        let containerCellRegistration = UICollectionView.CellRegistration<UICollectionViewListCell, OutlineItem> { (cell, _, menuItem) in
            var contentConfiguration = cell.defaultContentConfiguration()
            contentConfiguration.text = menuItem.title
            contentConfiguration.textProperties.font = .preferredFont(forTextStyle: .headline)
            
            let disclosureOptions = UICellAccessory.OutlineDisclosureOptions(style: .header)
            cell.accessories = [.outlineDisclosure(options: disclosureOptions)]
            cell.contentConfiguration = contentConfiguration
            cell.backgroundConfiguration = .clear()
        }
        
        let cellRegistration = UICollectionView.CellRegistration<UICollectionViewListCell, OutlineItem> { cell, _, menuItem in
            var contentConfiguration = cell.defaultContentConfiguration()
            contentConfiguration.text = menuItem.title
            cell.contentConfiguration = contentConfiguration
            cell.backgroundConfiguration = .clear()
        }
        
        let dataSource = CollectionDataSource(collectionView: outlineCollectionView) { (collectionView, indexPath, item) -> UICollectionViewCell? in
            collectionView.dequeueConfiguredReusableCell(
                using: item.subitems.isEmpty ? cellRegistration : containerCellRegistration,
                for: indexPath,
                item: item
            )
        }
        
        return dataSource
    }()
    
    private lazy var menuItems: [OutlineItem] = {
        [
            OutlineItem(title: "Compositional Layout", subitems: [
                OutlineItem(title: "Getting Started", subitems: [
                    OutlineItem(title: "Grid", viewController: GridViewController.self),
                    OutlineItem(title: "Inset Items Grid", viewController: InsetItemsGridViewController.self),
                    OutlineItem(title: "Two-Column Grid", viewController: TwoColumnViewController.self),
                    OutlineItem(title: "Adaptive Sections", viewController: AdaptiveSectionsViewController.self)
                ]),
                OutlineItem(title: "Advanced Layouts", subitems: [
                    OutlineItem(title: "Section Headers/Footers", viewController: SectionHeadersFootersViewController.self),
                    OutlineItem(title: "Pinned Section Headers", viewController: PinnedSectionHeaderFooterViewController.self),
                    OutlineItem(title: "Section Background Decoration", viewController: SectionDecorationViewController.self),
                    OutlineItem(title: "Nested Groups", viewController: NestedGroupsViewController.self),
                    OutlineItem(title: "Orthogonal Section Behaviors", viewController: OrthogonalScrollBehaviorViewController.self)
                ]),
                OutlineItem(title: "Conference App", subitems: [
                    OutlineItem(title: "Videos", viewController: ConferenceVideoSessionsViewController.self),
                    OutlineItem(title: "News", viewController: ConferenceNewsFeedViewController.self)
                ])
            ]),
            OutlineItem(title: "Diffable Data Source", subitems: [
                OutlineItem(title: "Mountains Search", viewController: MountainsViewController.self),
                OutlineItem(title: "Settings: Wi-Fi", viewController: WiFiSettingsViewController.self),
                OutlineItem(title: "UITableView: Editing", viewController: TableViewEditingViewController.self)
            ]),
            OutlineItem(title: "Outlines", subitems: [
                OutlineItem(title: "Emoji Explorer", viewController: EmojiExplorerViewController.self)
            ])
        ]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialSnapshot()
    }
    
    func initialSnapshot() {
        var snapshot = NSDiffableDataSourceSectionSnapshot<OutlineItem>()

        func addItems(_ menuItems: [OutlineItem], to parent: OutlineItem?) {
            snapshot.append(menuItems, to: parent)
            
            for menuItem in menuItems where !menuItem.subitems.isEmpty {
                addItems(menuItem.subitems, to: menuItem)
            }
        }
        
        addItems(menuItems, to: nil)
        
        self.dataSource.apply(snapshot, to: 0, animatingDifferences: false)
    }
    
    private func generateLayout() -> UICollectionViewLayout {
        let listConfiguration = UICollectionLayoutListConfiguration(appearance: .sidebar)
        let layout = UICollectionViewCompositionalLayout.list(using: listConfiguration)
        
        return layout
    }
}

// MARK: - Collection View Delegates

extension ModernCollectionViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        guard let menuItem = self.dataSource.itemIdentifier(for: indexPath) else { return }
        
        if let viewController = menuItem.outlineViewController {
            navigationController?.pushViewController(viewController.init(), animated: true)
        }
    }
}
