//
//  ViewController.swift
//  ReactiveMVVM
//
//  Created by Manuel Espeso on 14/1/22.
//

import UIKit

class ViewController: UIViewController {
    /// El contenido de ambos casos tiene que ser hasable
    private typealias TableDataSource = UITableViewDiffableDataSource<Int /*Section*/, Components /*Content*/>
    
    private struct Components: Hashable {
        /// UITableViewDiffableDataSource necesita parametros de entrada unicos, no pueden repetirse, y con uuid hacemos único cada item, aunque su contenido sea idéntico.
        let uuid = UUID()
        let title: String
        let vc: UIViewController
    }
    
    private var components: [Components] = [
        Components(title: "Combine", vc: CombineViewController()),
        Components(title: "Custom Action Sheet's", vc: CustomActionSheetDemoViewController()),
        Components(title: "Shapes", vc: ShapesViewController()),
        Components(title: "Collection View's", vc: ModernCollectionViewController())
    ]
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView(frame: .zero, style: .insetGrouped)
        tableView.delegate = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        tableView.frame = view.bounds
        view.addSubview(tableView)
        
        return tableView
    }()
    
    private lazy var tableViewDS: TableDataSource = {
        let diffDataSource = TableDataSource(tableView: tableView, cellProvider: { (tableView, indexPath, model) -> UITableViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.selectionStyle = .none
            cell.textLabel?.text = model.title
            
            return cell
        })
        
        return diffDataSource
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Components"
        updateDatasource()
    }
    
    private func updateDatasource() {
        var snapshot = NSDiffableDataSourceSnapshot<Int, Components>()
        snapshot.appendSections([0])
        snapshot.appendItems(components)
        
        tableViewDS.apply(snapshot, animatingDifferences: true, completion: nil)
    }
}

// MARK: TableView Protocols

extension ViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let component = tableViewDS.itemIdentifier(for: indexPath) else { return }
        navigationController?.pushViewController(component.vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView,
                   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        guard let fruit = tableViewDS.itemIdentifier(for: indexPath),
              let fruitIndex = components.firstIndex(of: fruit) else { return nil }
        
        let lockedAction = UIContextualAction(style: .destructive, title: "Delete") { [weak self] (_, _, _) in
            self?.components.remove(at: fruitIndex)
            self?.updateDatasource()
        }
        
        return UISwipeActionsConfiguration(actions: [lockedAction])
    }
}
