//
//  CustomActionSheetDemoViewController.swift
//  SwiftThings
//
//  Created by Manuel Espeso on 14/4/22.
//

import UIKit

class CustomActionSheetDemoViewController: UIViewController {

    @IBAction private func customDententButtonTapped(_ sender: Any) {
        let vc = CustomSheetDetentViewController()
        vc.loadViewIfNeeded()
        
        present(vc, animated: true)
    }
    
    @IBAction private func customBottomSheetButtonTapped(_ sender: Any) {
        let nib = UINib(nibName: "SimpleView", bundle: nil)
        guard let view = nib.instantiate(withOwner: self, options: nil).first as? UIView else { return }
        
        let vc = BottomSheetViewController(with: view)
        vc.modalPresentationStyle = .overCurrentContext
        
        present(vc, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
