//
//  SimpleView.swift
//  SwiftThings
//
//  Created by Manuel Espeso on 17/4/22.
//

import UIKit

class SimpleView: UIView {
    
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = "Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla FIN1"
        
        return label
    }()
    
    private lazy var textLabel2: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = "Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla FIN2"
        
        return label
    }()
    
    private lazy var textLabel3: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = "Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla FIN3"
        
        return label
    }()
    
    private lazy var textLabel4: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = "Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla FIN4"
        
        return label
    }()
    
    private lazy var textLabel5: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = "Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla FIN5"
        
        return label
    }()
    
    private lazy var textLabel6: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.text = "Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla Bla FIN6"
        
        return label
    }()
    
    @IBOutlet private weak var contentStackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let array = [
            textLabel,
            textLabel2,
            textLabel3,
            textLabel4,
            textLabel5,
            textLabel6
        ]
        
        array.forEach({ contentStackView.addArrangedSubview($0) })
    }
}
