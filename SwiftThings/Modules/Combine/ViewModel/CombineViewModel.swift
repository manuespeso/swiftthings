//
//  CombineViewModel.swift
//  ReactiveMVVM
//
//  Created by Manuel Espeso on 15/2/22.
//

import Foundation
import Combine

class CombineViewModel: LoadableObject {
    
    private var cancellables: Set<AnyCancellable> = []
    private let combineRepository: CombineRepository
    
    @Published private(set) var state: LoadableState<[User]> = .loading
    
    init(combineRepository: CombineRepository = CombineRepositoryDefault()) {
        self.combineRepository = combineRepository
    }
    
    func load() {
        /* MODO 1 (Si se quiere devolver un objeto fake o mockeado en vez de un error a la UI */
        combineRepository
            .fetchUsers()
            .map(LoadableState.loaded)
            // Devolver un objecto fake pero tambien se podria gestionar el error para lanzar errores desde un objeto generico
            //.catch { _ in Just(.loaded([User(name: "fake")])) }
            .replaceError(with: .loaded([User()]))
            .eraseToAnyPublisher()
            .assign(to: &$state)
        
        /* MODO 2 (Si se quiere propagar el error a la UI)
        combineRepository
            .fetchUsers()
            .convertToLoadedState()
            .assign(to: &$state)
        */
        
        /* MODO 3 (Si se quiere utilizar el resultado de la request en el ViewModel en vez de propagarlo a la UI)
         combineRepository
            .fetchUsers()
            .convertToResult()
            .sink { result in
                switch result {
                case .success(let users): print(users)
                case .failure(let error): print(error)
                }
            }
            .store(in: &cancellables)
         */
        
        /* MODO 4 (ZIP)
         Publishers.Zip(combineRepository.fetchUsers(), combineRepository.fetchUsers())
            .convertToResult()
            .sink(receiveValue: { result in
                switch result {
                case let .success((users, users2)):
                    print("wuao, magic", users)
                    print("wuao, magic 2", users2)
                case .failure(let error): break
                }
            })
            .store(in: &cancellables)
         */
    }
}
