//
//  CombineTableViewCell.swift
//  ReactiveMVVM
//
//  Created by Manuel Espeso on 6/3/22.
//

import UIKit

class CombineTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var userImageView: UIImageView!
    @IBOutlet private weak var userTitleLabel: UILabel!
    @IBOutlet private weak var userSalaryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureView(with user: User) {
        userTitleLabel.text = user.name
        userSalaryLabel.text = user.email
    }
}
