
import UIKit
import Combine

class CombineViewController: UIViewController {
    
    private typealias TableDataSource = UITableViewDiffableDataSource<Int, User>
    
    private let viewModel = CombineViewModel()
    private var cancellables: Set<AnyCancellable> = []
    
    @IBOutlet private weak var usersTableView: UITableView!
    @IBOutlet private weak var reactiveTextField: UITextField!
    @IBOutlet private weak var reactiveButton: UIButton!
    
    private lazy var tableViewDS: TableDataSource = {
        let diffDataSource = TableDataSource(tableView: usersTableView,
                                             cellProvider: { (tableView, indexPath, user) -> UITableViewCell? in
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
            cell.selectionStyle = .none
            
            if let cell = cell as? CombineTableViewCell {
                cell.configureView(with: user)
            }
            
            return cell
        })
        
        return diffDataSource
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.load()
                
        setupTableView()
        handleUsersState()
        handleReactiveTextField()
        handleReactiveButton()
    }
    
    private func setupTableView() {
        usersTableView.register(UINib(nibName: "CombineTableViewCell",
                                      bundle: nil),
                                forCellReuseIdentifier: "cell")
    }
    
    private func handleUsersState() {
        viewModel
            .$state
            .subscribe(on: DispatchQueue.global())
            .receive(on: DispatchQueue.main)
            .sink { [weak self] state in
                guard let self = self else { return }
                
                switch state {
                case .loading:
                    self.usersTableView.starLoading()
                case .failed(let error):
                    print("failed", error)
                case .loaded(let users):
                    self.usersTableView.stopLoading()
                    
                    var snapshot = NSDiffableDataSourceSnapshot<Int, User>()
                    snapshot.appendSections([0])
                    snapshot.appendItems(users)
                    
                    self.tableViewDS.apply(snapshot, animatingDifferences: true, completion: nil)
                }
            }
            .cancel()
    }
    
    private func handleReactiveTextField() {
        reactiveTextField.delegate = self
        
        /* UTILIZANDO LAS NOTIFICACIONES QUE TE OFRECE EL SISTEMA JUNTO A COMBINE */
        /*reactiveTextField
            .textPublisherDidChange
            .compactMap { !$0.isEmpty }
            .assign(to: \.isEnabled, on: reactiveButton)
            .store(in: &cancellables)

        reactiveTextField
            .textPublisherEndEditing
            .sink { value in
                print("VALOR", value)
            }
            .store(in: &cancellables)*/
        
        /* CREANDO UN PUBLISHER CUSTOMIZADO Y GESTIONANDO LOS EVENTOS DEL UICONTROL */
        reactiveTextField
            .publisher(for: .editingChanged)
            .compactMap { ($0 as? UITextField)?.text }
            .compactMap { !($0.isEmpty) }
            .assign(to: \.isEnabled, on: reactiveButton)
            .store(in: &cancellables)
        
        reactiveTextField
            .publisher(for: .editingChanged)
            .compactMap { $0 as? UITextField }
            .compactMap { $0.text }
            .sink(receiveValue: { print("VALOR", $0) })
            .store(in: &cancellables)
    }
    
    private func handleReactiveButton() {
        reactiveButton.isEnabled = false
        reactiveButton
            .publisher(for: .touchUpInside)
            .sink(receiveValue: { print("TAPPED", $0) })
            .store(in: &cancellables)
    }
}

extension CombineViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.reactiveTextField.endEditing(true)
        return false
    }
}
