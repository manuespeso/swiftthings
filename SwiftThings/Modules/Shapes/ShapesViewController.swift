//
//  ShapesViewController.swift
//  SwiftThings
//
//  Created by Manuel Espeso on 19/4/22.
//

import UIKit

class ShapesViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //renderShape()
        //renderPredefinedShapes()
    }
    
    private func renderShape() {
        //Shape lines part
        /*let path = UIBezierPath()
        path.move(to: .init(x: 200, y: 200)) //StartPoint
        path.addLine(to: .init(x: 380, y: 380)) //EndPoint of First Line and StartPoint for Second Line
        path.addLine(to: .init(x: 20, y: 380)) //EndPoint of Second Line*/
        
        //Shape QuadCurve part
        /*let path = UIBezierPath()
        path.move(to: .init(x: 100, y: 300))
        path.addQuadCurve(to: .init(x: 250, y: 300), controlPoint: .init(x: 175, y: 450))*/
        
        //Shape with curve part
        let path = UIBezierPath()
        path.move(to: .init(x: 50, y: 200))
        path.addCurve(
            to: .init(x: 200, y: 200),
            controlPoint1: .init(x: 80, y: 300),
            controlPoint2: .init(x: 150, y: 0)
        )
        
        //Shape part
        let shape = CAShapeLayer()
        shape.path = path.cgPath
        shape.lineWidth = 4.0
        shape.fillColor = UIColor.cyan.cgColor
        shape.strokeColor = UIColor.orange.cgColor
        view.layer.addSublayer(shape)
    }
    
    private func renderPredefinedShapes() {
        //Paths Part
        let rect = UIBezierPath(rect: .init(x: 50, y: 150, width: 50, height: 50))
        let oval = UIBezierPath(ovalIn: .init(x: 120, y: 220, width: 100, height: 50))
        let roundedRect = UIBezierPath(roundedRect: .init(x: 200, y: 300, width: 50, height: 50), cornerRadius: 10)
        //Shapes
        let rectShape = CAShapeLayer()
        rectShape.path = rect.cgPath
        rectShape.lineWidth = 4.0
        rectShape.fillColor = UIColor.clear.cgColor
        rectShape.strokeColor = UIColor.orange.cgColor
        view.layer.addSublayer(rectShape)

        let ovalShape = CAShapeLayer()
        ovalShape.path = oval.cgPath
        ovalShape.lineWidth = 4.0
        ovalShape.fillColor = UIColor.clear.cgColor
        ovalShape.strokeColor = UIColor.orange.cgColor
        view.layer.addSublayer(ovalShape)

        let roundedRectShape = CAShapeLayer()
        roundedRectShape.path = roundedRect.cgPath
        roundedRectShape.lineWidth = 4.0
        roundedRectShape.fillColor = UIColor.clear.cgColor
        roundedRectShape.strokeColor = UIColor.orange.cgColor
        
        view.layer.addSublayer(roundedRectShape)
    }
}
