//
//  CupHeadView.swift
//  SwiftThings
//
//  Created by Manuel Espeso on 19/4/22.
//

import UIKit

class CupHeadView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setupUI()
    }
    
    private func setupUI() {
        backgroundColor = .white
        
        let head = getHeadShape()
        let ear = getEarShape()
        let rightEye = getRightEyeShape()
        let leftEye = getLeftEyeShape()
        let nose = getNoseShape()
        let mouth = getMouthShape()
        let tongue = getTongueShape()
        let drinkingTube = getDrinkingTubeShape()
        
        layer.addSublayer(head)
        layer.addSublayer(leftEye)
        layer.addSublayer(rightEye)
        layer.addSublayer(ear)
        layer.addSublayer(nose)
        layer.addSublayer(mouth)
        layer.addSublayer(drinkingTube)
        mouth.addSublayer(tongue)
    }
    
    private func getHeadShape() -> CAShapeLayer {
        let path = UIBezierPath()
        path.move(to: .init(x: 84, y: 196))
        path.addQuadCurve(to: .init(x: 366, y: 100), controlPoint: .init(x: 205, y: 100))
        path.addCurve(
            to: .init(x: 84, y: 196),
            controlPoint1: .init(x: 500, y: 330),
            controlPoint2: .init(x: 120, y: 430)
        )
        path.close()
        
        let headShape = CAShapeLayer()
        headShape.path = path.cgPath
        headShape.lineWidth = 5
        headShape.strokeColor = UIColor.black.cgColor
        headShape.fillColor =  UIColor.clear.cgColor
        
        return headShape
    }
    
    private func getRightEyeShape() -> CAShapeLayer {
        let rightEyeWrapPath = UIBezierPath()
        rightEyeWrapPath.move(to: .init(x: 392, y: 165))
        rightEyeWrapPath.addCurve(
            to: .init(x: 317, y: 175),
            controlPoint1: .init(x: 400, y: 200),
            controlPoint2: .init(x: 330, y: 230)
        )
        
        let rightEyeWrapShape = CAShapeLayer()
        rightEyeWrapShape.path = rightEyeWrapPath.cgPath
        rightEyeWrapShape.lineWidth = 5
        rightEyeWrapShape.strokeColor = UIColor.black.cgColor
        rightEyeWrapShape.fillColor = UIColor.clear.cgColor
        
        let eyePath = UIBezierPath()
        eyePath.move(to: .init(x: 345, y: 135))
        eyePath.addQuadCurve(to: .init(x: 350, y: 155), controlPoint: .init(x: 320, y: 150))
        eyePath.addCurve(
            to: .init(x: 315, y: 150),
            controlPoint1: .init(x: 355, y: 190),
            controlPoint2: .init(x: 315, y: 195)
        )
        eyePath.addCurve(
            to: .init(x: 345, y: 135),
            controlPoint1: .init(x: 300, y: 100),
            controlPoint2: .init(x: 340, y: 100)
        )
        
        let eyeShape = CAShapeLayer()
        eyeShape.path = eyePath.cgPath
        eyeShape.strokeColor = UIColor.black.cgColor
        eyeShape.lineWidth = 1
        eyeShape.fillColor = UIColor.black.cgColor
        rightEyeWrapShape.addSublayer(eyeShape)
        
        return rightEyeWrapShape
    }
    
    private func getLeftEyeShape() -> CAShapeLayer {
        let leftEyeWrapPath = UIBezierPath()
        leftEyeWrapPath.move(to: .init(x: 135, y: 162))
        leftEyeWrapPath.addCurve(
            to: .init(x: 300, y: 105),
            controlPoint1: .init(x: 140, y: 280),
            controlPoint2: .init(x: 380, y: 280)
        )
        
        let leftEyeWrapShape = CAShapeLayer()
        leftEyeWrapShape.path = leftEyeWrapPath.cgPath
        leftEyeWrapShape.lineWidth = 5
        leftEyeWrapShape.strokeColor = UIColor.black.cgColor
        leftEyeWrapShape.fillColor =  UIColor.clear.cgColor
        
        let eyePath = UIBezierPath()
        eyePath.move(to: .init(x: 233, y: 155))
        eyePath.addQuadCurve(to: .init(x: 242, y: 185), controlPoint: .init(x: 200, y: 180))
        eyePath.addCurve(
            to: .init(x: 190, y: 180),
            controlPoint1: .init(x: 250, y: 240),
            controlPoint2: .init(x: 215, y: 235)
        )
        eyePath.addCurve(
            to: .init(x: 233, y: 155),
            controlPoint1: .init(x: 170, y: 120),
            controlPoint2: .init(x: 230, y: 130)
        )
        
        let eyeShape = CAShapeLayer()
        eyeShape.path = eyePath.cgPath
        eyeShape.lineWidth = 1
        eyeShape.strokeColor = UIColor.black.cgColor
        eyeShape.fillColor = UIColor.black.withAlphaComponent(1).cgColor
        leftEyeWrapShape.addSublayer(eyeShape)
        
        return leftEyeWrapShape
    }
    
    private func getNoseShape() -> CAShapeLayer {
        let nosePath = UIBezierPath(ovalIn: CGRect(x: 310, y: 180, width: 52, height: 30))
        
        let noseShape = CAShapeLayer()
        noseShape.path = nosePath.cgPath
        noseShape.strokeColor = UIColor.black.cgColor
        noseShape.lineWidth = 4
        noseShape.fillColor = UIColor.red.cgColor
        
        return noseShape
    }
    
    private func getEarShape() -> CAShapeLayer {
        let path = UIBezierPath(
            arcCenter: .init(x: 100, y: 260),
            radius: 55.0,
            startAngle: 1.4 * CGFloat.pi,
            endAngle: CGFloat.pi / 4,
            clockwise: false
        )
        
        let earShape = CAShapeLayer()
        earShape.path = path.cgPath
        earShape.lineWidth = 5
        earShape.strokeColor = UIColor.black.cgColor
        earShape.fillColor =  UIColor.clear.cgColor
        
        let fullEar = CAShapeLayer()
        fullEar.addSublayer(earShape)
        
        let path2 = UIBezierPath(
            arcCenter: .init(x: 100, y: 260),
            radius: 20.0,
            startAngle: 1.4 * CGFloat.pi,
            endAngle: CGFloat.pi / 4,
            clockwise: false
        )
        
        let innerEar = CAShapeLayer()
        innerEar.path = path2.cgPath
        innerEar.lineWidth = 5
        innerEar.strokeColor = UIColor.black.cgColor
        innerEar.fillColor =  UIColor.clear.cgColor
        fullEar.addSublayer(innerEar)
        
        return fullEar
    }
    
    private func getMouthShape() -> CAShapeLayer {
        let path = UIBezierPath()
        path.move(to: .init(x: 310, y: 250))
        path.addCurve(
            to: .init(x: 175, y: 250),
            controlPoint1: .init(x: 330, y: 335),
            controlPoint2: .init(x: 125, y: 325)
        )
        path.addCurve(
            to: .init(x: 310, y: 250),
            controlPoint1: .init(x: 195, y: 245),
            controlPoint2: .init(x: 260, y: 265)
        )
        
        let mouthShape = CAShapeLayer()
        mouthShape.path = path.cgPath
        mouthShape.lineWidth = 3
        mouthShape.strokeColor = UIColor.black.cgColor
        mouthShape.fillColor =  UIColor.black.cgColor
        
        return mouthShape
    }
    
    private func getTongueShape() -> CAShapeLayer {
        let path = UIBezierPath()
        path.move(to: .init(x: 175, y: 290))
        path.addCurve(
            to: .init(x: 230, y: 285),
            controlPoint1: .init(x: 160, y: 250),
            controlPoint2: .init(x: 210, y: 250)
        )
        path.addCurve(
            to: .init(x: 270, y: 305),
            controlPoint1: .init(x: 210, y: 260),
            controlPoint2: .init(x: 260, y: 265)
        )
        path.addQuadCurve(to: .init(x: 175, y: 290), controlPoint: .init(x: 210, y: 320))
        
        let tongueShape = CAShapeLayer()
        tongueShape.path = path.cgPath
        tongueShape.lineWidth = 2
        tongueShape.strokeColor = UIColor.black.cgColor
        tongueShape.fillColor = UIColor.red.cgColor
        
        return tongueShape
    }
    
    private func getDrinkingTubeShape() -> CAShapeLayer {
        //Wraper Shape
        let wrapPath = UIBezierPath()
        wrapPath.move(to: .init(x: 170, y: 145))
        wrapPath.addLine(to: .init(x: 125, y: 70))
        wrapPath.addLine(to: .init(x: 5, y: 110))
        wrapPath.addQuadCurve(to: .init(x: 35, y: 175), controlPoint: .init(x: -5, y: 160))
        wrapPath.addQuadCurve(to: .init(x: 95, y: 150), controlPoint: .init(x: 60, y: 165))
        wrapPath.addLine(to: .init(x: 110, y: 175))
        
        let tubeWrapShape = CAShapeLayer()
        tubeWrapShape.path = wrapPath.cgPath
        tubeWrapShape.lineWidth = 5
        tubeWrapShape.strokeColor = UIColor.black.cgColor
        tubeWrapShape.fillColor = UIColor.clear.cgColor
        //Red Line 1
        let redLinePath1 = UIBezierPath()
        redLinePath1.move(to: .init(x: 160, y: 130))
        redLinePath1.addQuadCurve(to: .init(x: 105, y: 170), controlPoint: .init(x: 120, y: 130))
        redLinePath1.addCurve(
            to: .init(x: 145, y: 105),
            controlPoint1: .init(x: 90, y: 140),
            controlPoint2: .init(x: 80, y: 120)
        )
        redLinePath1.close()
        
        let redLineShape1 = CAShapeLayer()
        redLineShape1.path = redLinePath1.cgPath
        redLineShape1.lineWidth = 3
        redLineShape1.strokeColor = UIColor.black.cgColor
        redLineShape1.fillColor = UIColor.red.cgColor
        tubeWrapShape.addSublayer(redLineShape1)
        //Red Line 2
        let redLinePath2 = UIBezierPath()
        redLinePath2.move(to: .init(x: 70, y: 160))
        redLinePath2.addLine(to: .init(x: 70, y: 90))
        redLinePath2.addLine(to: .init(x: 40, y: 100))
        redLinePath2.addLine(to: .init(x: 40, y: 170))
        redLinePath2.close()
        
        let redLineShape2 = CAShapeLayer()
        redLineShape2.path = redLinePath2.cgPath
        redLineShape2.lineWidth = 3
        redLineShape2.strokeColor = UIColor.black.cgColor
        redLineShape2.fillColor = UIColor.red.cgColor
        tubeWrapShape.addSublayer(redLineShape2)
        //Middle Line Shape
        let middleLinePath = UIBezierPath()
        middleLinePath.move(to: .init(x: 97, y: 155))
        middleLinePath.addQuadCurve(to: .init(x: 125, y: 70), controlPoint: .init(x: 80, y: 80))
        
        let middleLineShape = CAShapeLayer()
        middleLineShape.path = middleLinePath.cgPath
        middleLineShape.lineWidth = 3
        middleLineShape.strokeColor = UIColor.black.cgColor
        middleLineShape.fillColor = UIColor.clear.cgColor
        tubeWrapShape.addSublayer(middleLineShape)
        
        return tubeWrapShape
    }
}
