//
//  HouseView.swift
//  SwiftThings
//
//  Created by Manuel Espeso on 19/4/22.
//

import UIKit

class HouseView: UIView {
    
    let paidHousePercentaje: Double = 63
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        let houseLayer = getHouseShape()
        let proportionalHouseLayer = getProportionalHouseShape()
        layer.addSublayer(houseLayer)
        houseLayer.addSublayer(proportionalHouseLayer)
    }
    
    private func getHouseShape() -> CAShapeLayer {
        let startXPoint = self.frame.size.width / 2
        
        let path = UIBezierPath()
        path.move(to: .init(x: startXPoint, y: 0))
        path.addLine(to: .init(x: startXPoint + 28, y: 28))
        path.addLine(to: .init(x: startXPoint + 20, y: 28))
        path.addLine(to: .init(x: startXPoint + 20, y: 56))
        path.addLine(to: .init(x: startXPoint - 20, y: 56))
        path.addLine(to: .init(x: startXPoint - 20, y: 28))
        path.addLine(to: .init(x: startXPoint - 28, y: 28))
        path.addLine(to: .init(x: startXPoint, y: 0))
        path.close()
        
        let headShape = CAShapeLayer()
        headShape.path = path.cgPath
        headShape.lineWidth = 2
        headShape.strokeColor = UIColor.systemBlue.cgColor
        headShape.fillColor = UIColor.clear.cgColor
        
        let fillCircleAnimation = CABasicAnimation(keyPath: "strokeEnd")
        fillCircleAnimation.fromValue = 0
        fillCircleAnimation.toValue = 1
        fillCircleAnimation.duration = 3
        fillCircleAnimation.fillMode = .forwards
        fillCircleAnimation.isRemovedOnCompletion = false
        fillCircleAnimation.repeatCount = .greatestFiniteMagnitude
        
        headShape.add(fillCircleAnimation, forKey: "fillCircleAnimation")
        
        return headShape
    }
    
    private func getProportionalHouseShape() -> CAShapeLayer {
        // 55 bottom Y asix && 0 == top Y asix
        // 55 == 100% && 0 == 0%
        let proportionalHeight = (paidHousePercentaje * 55) / 100
        let yPosition = 55 - proportionalHeight
        let startXPoint = self.frame.size.width / 2
        
        let path = UIBezierPath()
        path.move(to: .init(x: startXPoint - 18, y: 54))
        path.addLine(to: .init(x: startXPoint + 18, y: 54))
        
        if proportionalHeight <= 28 {
            path.addLine(to: .init(x: startXPoint + 18, y: yPosition))
            path.addLine(to: .init(x: startXPoint - 18, y: yPosition))
            path.addLine(to: .init(x: startXPoint - 18, y: 54))
            path.close()
        } else {
            path.addLine(to: .init(x: startXPoint + 18, y: 26))
            path.addLine(to: .init(x: startXPoint + 23, y: 26))
            path.addLine(to: .init(x: startXPoint + (yPosition - 2), y: (yPosition + 1)))
            path.addLine(to: .init(x: startXPoint - (yPosition - 2), y: (yPosition + 1)))
            path.addLine(to: .init(x: startXPoint - 23, y: 26))
            path.addLine(to: .init(x: startXPoint - 18, y: 26))
            path.addLine(to: .init(x: startXPoint - 18, y: 54))
            path.close()
        }
        
        let fillHouseShape = CAShapeLayer()
        fillHouseShape.path = path.cgPath
        fillHouseShape.lineWidth = 1
        fillHouseShape.strokeColor = UIColor.clear.cgColor
        fillHouseShape.fillColor = UIColor.cyan.cgColor
        
        return fillHouseShape
    }
}
