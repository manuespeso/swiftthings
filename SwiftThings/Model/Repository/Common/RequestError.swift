//
//  RequestError.swift
//  ReactiveMVVM
//
//  Created by Manuel Espeso on 21/2/22.
//

enum RequestError: Error {
    case commonError
    case notInternet
    case notData
    case incorrectURL
}
