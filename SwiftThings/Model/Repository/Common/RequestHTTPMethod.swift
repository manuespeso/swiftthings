//
//  RequestHTTPMethod.swift
//  ReactiveMVVM
//
//  Created by Manuel Espeso on 23/2/22.
//

enum RequestHTTPMethod: String {
    case get = "GET"
    case post = "POST"
}
