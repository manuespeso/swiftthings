//
//  User.swift
//  ReactiveMVVM
//
//  Created by Manuel Espeso on 21/2/22.
//

struct User: Codable, Hashable {
    let name: String
    let email: String
    
    init() {
        self.name = "fake"
        self.email = "fake@fake.es"
    }
}
