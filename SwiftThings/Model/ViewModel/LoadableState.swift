//
//  LoadableState.swift
//  ReactiveMVVM
//
//  Created by Manuel Espeso on 21/2/22.
//

enum LoadableState<Value> {
    case loading
    case failed(Error)
    case loaded(Value)
}
