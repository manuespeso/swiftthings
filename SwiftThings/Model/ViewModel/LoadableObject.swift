//
//  LoadableObject.swift
//  ReactiveMVVM
//
//  Created by Manuel Espeso on 21/2/22.
//

import Combine

protocol LoadableObject: ObservableObject {
    associatedtype Output
    var state: LoadableState<Output> { get }
    func load()
}
