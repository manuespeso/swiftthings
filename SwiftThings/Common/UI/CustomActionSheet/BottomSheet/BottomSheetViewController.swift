//
//  BottomSheetViewController.swift
//  SwiftThings
//
//  Created by Manuel Espeso on 14/4/22.
//

import UIKit

class BottomSheetViewController: UIViewController {
    
    //-----------------------------------------------------------------------------
    // MARK: - Private properties
    //-----------------------------------------------------------------------------
    
    private var containerViewHeightConstraint: NSLayoutConstraint?
    private var containerViewBottomConstraint: NSLayoutConstraint?
    private var initialHeight: CGFloat = 0
    private var dismissibleHeight: CGFloat = 0
    private var currentContainerHeight: CGFloat = 0
    private let maximumContainerHeight: CGFloat = UIScreen.main.bounds.height - (UIApplication.shared.keyWindow?.safeAreaInsets.top ?? 64)
    
    private lazy var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 16
        view.clipsToBounds = true
        
        return view
    }()
    
    private lazy var dimmedView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0
        
        return view
    }()
    
    private lazy var grabbedView: UIView = {
        let view = UIView()
        view.isUserInteractionEnabled = false
        view.backgroundColor = .gray
        view.layer.cornerRadius = 2
        view.clipsToBounds = true
        
        return view
    }()
    
    //-----------------------------------------------------------------------------
    // MARK: - Lifecycle
    //-----------------------------------------------------------------------------
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupConstraints()
        setupGestures()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        animatePresentDimmedView()
        animatePresentContainerView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if containerViewHeightConstraint == nil {
            let contentViewHeight = containerView.frame.size.height
            
            containerViewHeightConstraint = containerView.heightAnchor.constraint(equalToConstant: contentViewHeight)
            containerViewHeightConstraint?.isActive = true
            containerViewBottomConstraint = containerView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: contentViewHeight)
            containerViewBottomConstraint?.isActive = true
            
            initialHeight = contentViewHeight
            dismissibleHeight = contentViewHeight - 100
            currentContainerHeight = contentViewHeight
        }
    }
    
    init(with view: UIView) {
        super.init(nibName: nil, bundle: nil)
        
        view.translatesAutoresizingMaskIntoConstraints = false
        containerView.addSubview(view)
        
        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: containerView.topAnchor),
            view.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            view.leadingAnchor.constraint(equalTo: containerView.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
        ])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //-----------------------------------------------------------------------------
    // MARK: - PRIVATE
    //-----------------------------------------------------------------------------
    
    private func setupView() {
        view.backgroundColor = .clear
    }
    
    private func setupConstraints() {
        view.addSubview(dimmedView)
        view.addSubview(containerView)
        view.addSubview(grabbedView)
        
        dimmedView.translatesAutoresizingMaskIntoConstraints = false
        containerView.translatesAutoresizingMaskIntoConstraints = false
        grabbedView.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            dimmedView.topAnchor.constraint(equalTo: view.topAnchor),
            dimmedView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            dimmedView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            dimmedView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            grabbedView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 6),
            grabbedView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            grabbedView.widthAnchor.constraint(equalToConstant: 35),
            grabbedView.heightAnchor.constraint(equalToConstant: 4)
        ])
    }
    
    private func setupGestures() {
        let containerPanGesture = UIPanGestureRecognizer(target: self, action: #selector(handlePanGesture(gesture:)))
        containerView.addGestureRecognizer(containerPanGesture)
        
        let dimmedTapGesture = UITapGestureRecognizer(target: self, action: #selector(handleDimmedGesture))
        dimmedView.addGestureRecognizer(dimmedTapGesture)
    }
    
    private func animatePresentContainerView() {
        // Update bottom constraint in animation block
        UIView.animate(withDuration: 0.3) {
            self.containerViewBottomConstraint?.constant = 0
            // Call this to trigger refresh constraint
            self.view.layoutIfNeeded()
        }
    }
    
    private func animatePresentDimmedView() {
        UIView.animate(withDuration: 0.4) { self.dimmedView.alpha = 0.6 }
    }
    
    private func animateDismissView() {
        // hide main container view by updating bottom constraint in animation block
        UIView.animate(withDuration: 0.3) {
            self.containerViewBottomConstraint?.constant = self.currentContainerHeight
            // call this to trigger refresh constraint
            self.view.layoutIfNeeded()
        }
        // hide blur view
        UIView.animate(withDuration: 0.4) {
            self.dimmedView.alpha = 0
        } completion: { _ in
            self.dismiss(animated: false)
        }
    }
    
    private func animateContainerHeight(_ height: CGFloat) {
        UIView.animate(withDuration: 0.4) {
            // Update container height
            self.containerViewHeightConstraint?.constant = height
            // Call this to trigger refresh constraint
            self.view.layoutIfNeeded()
        }
        // Save current height
        currentContainerHeight = height
    }
    
    @objc private func handlePanGesture(gesture: UIPanGestureRecognizer) {
        let translation = gesture.translation(in: view)
        // Get drag direction
        let isDraggingDown = translation.y > 0
        // New height is based on value of dragging plus current container height
        let newHeight = currentContainerHeight - translation.y
        
        switch gesture.state {
            case .changed:
                if newHeight < maximumContainerHeight {
                    containerViewHeightConstraint?.constant = newHeight
                    view.layoutIfNeeded()
                }
            case .ended:
                // Condition 1: If new height is below min, dismiss controller
                if newHeight < dismissibleHeight {
                    self.animateDismissView()
                } else if newHeight < initialHeight || newHeight < maximumContainerHeight && isDraggingDown {
                    // Condition 2: If new height is below DEFAULT OR below MAX and going DOWN, animate back to default
                    animateContainerHeight(initialHeight)
                } else if newHeight > initialHeight && !isDraggingDown {
                    // Condition 3: If new height is below max and going up, set to max height at top
                    animateContainerHeight(maximumContainerHeight)
                }
            default: break
        }
    }
    
    @objc private func handleDimmedGesture() {
        animateDismissView()
    }
}
