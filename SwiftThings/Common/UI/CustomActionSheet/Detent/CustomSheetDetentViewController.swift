//
//  CustomSheetDetentViewController.swift
//  SwiftThings
//
//  Created by Manuel Espeso on 14/4/22.
//

import UIKit

class CustomSheetDetentViewController: UIViewController {
    
    private lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .systemCyan
        view.addSubview(tableView)
        
        modalPresentationStyle = .custom
        transitioningDelegate = self
        
        NSLayoutConstraint.activate([
            tableView.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            tableView.topAnchor.constraint(equalTo: self.view.topAnchor),
            tableView.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor)
        ])
    }
}

// MARK: - Transition Delegate

extension CustomSheetDetentViewController: UIViewControllerTransitioningDelegate {
    
    func presentationController(
        forPresented presented: UIViewController,
        presenting: UIViewController?,
        source: UIViewController
    ) -> UIPresentationController? {
        let controller: UISheetPresentationController = .init(presentedViewController: presented, presenting: presenting)
        let small: UISheetPresentationController.Detent = ._detent(withIdentifier: "small", constant: 75)
        
        controller.delegate = self
        controller.detents = [small, .medium(), .large()]
//        controller.largestUndimmedDetentIdentifier = .medium
        //controller.prefersScrollingExpandsWhenScrolledToEdge = false
//        controller.selectedDetentIdentifier = .medium
        controller.prefersGrabberVisible = true
        
        return controller
    }
}

// MARK: - Presentation Delegate

extension CustomSheetDetentViewController: UISheetPresentationControllerDelegate {
    
    func sheetPresentationControllerDidChangeSelectedDetentIdentifier(_ sheetPresentationController: UISheetPresentationController) {
        switch sheetPresentationController.selectedDetentIdentifier?.rawValue {
            case "small":
                print("pequeño")
            case UISheetPresentationController.Detent.Identifier.medium.rawValue:
                print("mediano")
            case UISheetPresentationController.Detent.Identifier.large.rawValue:
                print("largo")
            default: break
        }
    }
}

// MARK: - Table View Delegate

extension CustomSheetDetentViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.backgroundColor = .systemOrange
        cell.textLabel?.text = "Title"
        cell.textLabel?.textColor = .white
        return cell
    }
}
