//
//  SkeletonView.swift
//  ReactiveMVVM
//
//  Created by Manuel Espeso on 6/3/22.
//

import UIKit

class SkeletonView: UIView {
    
    private let startLocations: [NSNumber] = [-1.0, -0.5, 0.0]
    private let endLocations: [NSNumber] = [1.0, 1.5, 2.0]
    
    private let gradientBackgroundColor: CGColor = UIColor(white: 0.25, alpha: 0.4).cgColor
    private let gradientMovingColor: CGColor = UIColor(white: 0.75, alpha: 0.6).cgColor
    
    private let movingAnimationDuration: CFTimeInterval = 0.5
    private let delayBetweenAnimationLoops: CFTimeInterval = 0.7
    
    lazy var gradientLayer : CAGradientLayer = {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.colors = [
            gradientBackgroundColor,
            gradientMovingColor,
            gradientBackgroundColor
        ]
        gradientLayer.locations = startLocations
        layer.addSublayer(gradientLayer)
        return gradientLayer
    }()
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = bounds
    }
    
    /**
     Start the gradient animation.
     */
    func startAnimating() {
        let animation = CABasicAnimation(keyPath: "locations")
        animation.fromValue = startLocations
        animation.toValue = endLocations
        animation.duration = movingAnimationDuration
        animation.timingFunction = CAMediaTimingFunction(name: .easeInEaseOut)
        
        let animationGroup = CAAnimationGroup()
        animationGroup.duration = movingAnimationDuration + delayBetweenAnimationLoops
        animationGroup.animations = [animation]
        animationGroup.repeatCount = .infinity
        gradientLayer.add(animationGroup, forKey: animation.keyPath)
    }
    
    /**
     Stop the gradient animation
     - Note: This methods doesn't remove the gradient view from the superlayer
     so its visiable after this method was called
     */
    func stopAnimating() {
        gradientLayer.removeAllAnimations()
    }
}
