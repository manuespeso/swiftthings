//
//  ItemSkeleton.swift
//  ReactiveMVVM
//
//  Created by Manuel Espeso on 6/3/22.
//

import UIKit

protocol ItemSkeletonable {
    /// Views that must be hidden/visible during loading process
    var viewsThatMustBeHidden: [UIView] { get }
    /// View that will be used as reference to define loading anchor.
    var referenceView: UIView? { get }
}
/// Make all UIView's can access to properties in BalanceItemSkeletonable and his extension (starLoading & stopLoading)
extension UIView: ItemSkeletonable {}
/// Added where condition for access to UIView properties
extension ItemSkeletonable where Self: UIView {
    
    var viewsThatMustBeHidden: [UIView] {
        self.subviews
    }
    
    var referenceView: UIView? {
        self
    }
    
    func starLoading() {
        self.viewsThatMustBeHidden.forEach { $0.isHidden = true }
        
        let referenceView = self.referenceView ?? self
        let gradientView = SkeletonView(frame: .zero)
        gradientView.clipsToBounds = true
        gradientView.layer.cornerRadius = 4.0
        gradientView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(gradientView)
        
        NSLayoutConstraint.activate([
            gradientView.heightAnchor.constraint(equalToConstant: referenceView.frame.height),
            gradientView.centerXAnchor.constraint(equalTo: referenceView.centerXAnchor),
            gradientView.leadingAnchor.constraint(equalTo: referenceView.leadingAnchor),
            gradientView.trailingAnchor.constraint(equalTo: referenceView.trailingAnchor)
        ])
        
        referenceView.layoutIfNeeded()
        gradientView.alpha = 0.08
        
        UIView.animate(withDuration: 0.5, animations: {
            gradientView.alpha = 1
        })
        
        gradientView.startAnimating()
    }
    
    func stopLoading() {
        let skeletonsViews = subviews.compactMap { $0 as? SkeletonView }
        
        UIView.animate(withDuration: 0.24, animations: {
            self.viewsThatMustBeHidden.forEach { $0.isHidden = false }
            skeletonsViews.forEach { $0.alpha = 0 }
        }) { _ in
            skeletonsViews.forEach {
                $0.stopAnimating()
                $0.removeFromSuperview()
            }
        }
    }
}
