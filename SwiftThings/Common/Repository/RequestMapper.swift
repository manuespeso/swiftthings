//
//  RequestMapper.swift
//  ReactiveMVVM
//
//  Created by Manuel Espeso on 21/2/22.
//

import Foundation

class RequestMapper {
    
    static let shared = RequestMapper()
    
    private init() {}
    
    func handleRequestError(_ error: Error) -> RequestError {
        switch (error as? URLError)?.code {
        case URLError.Code.notConnectedToInternet:
            return .notInternet
        default:
            return .commonError
        }
    }
}
