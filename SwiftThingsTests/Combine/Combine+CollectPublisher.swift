//
//  Combine+CollectPublisher.swift
//  ReactiveMVVMTests
//
//  Created by Manuel Espeso on 25/2/22.
//

import Combine

extension Published.Publisher {
    func collectNext(_ count: Int) -> AnyPublisher<[Output], Never> {
        self.dropFirst() // sin este atributo, combine devuelve el valor asignado por defecto
            .collect(count)
            .first()
            .eraseToAnyPublisher()
    }
}
