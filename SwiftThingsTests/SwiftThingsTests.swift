//
//  ReactiveMVVMTests.swift
//  ReactiveMVVMTests
//
//  Created by Manuel Espeso on 25/2/22.
//

import XCTest

@testable import SwiftThings
class SwiftThingsTests: XCTestCase {
    
    private var viewModel: CombineViewModel?
    
    override func setUp() {
        self.viewModel = CombineViewModel()
    }
    
    func testCombineRepositoryRequest() throws {
        let repository = CombineRepositoryDefault()
        let result = try awaitPublisher(repository.fetchUsers())
        
        result.forEach { XCTAssertNotNil($0.name) }
        XCTAssertNotEqual(result.count, 0)
    }
    
    func testPublisherState() throws {
        guard let viewModel = viewModel else { return }
        viewModel.load()
        // Here we collect the first [Users] values that our published property emitted:
        let usersPublisher = viewModel.$state.collectNext(1)
        let usersArray = try awaitPublisher(usersPublisher)
        
        switch usersArray.first {
        case .loaded(let users):
            users.forEach { XCTAssertNotNil($0.name) }
            XCTAssertEqual(users.count, 10)
            XCTAssertEqual(users.first?.name, "Leanne Graham")
            XCTAssertEqual(users.last?.name, "Clementina DuBuque")
        default: break
        }
    }
}
